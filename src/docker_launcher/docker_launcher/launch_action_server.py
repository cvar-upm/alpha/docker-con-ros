import subprocess

import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node

from docker_launcher_interfaces.action import DockerLauncher


class LaunchActionServer(Node):

    def __init__(self):
        super().__init__('launch_action_server')
        self._action_server = ActionServer(
            self,
            DockerLauncher,
            'DockerLauncher',
            self.execute_callback)

    def execute_callback(self, goal_handle):
        self.get_logger().info('Executing goal...')
        self.get_logger().info(goal_handle.request.request)
        docker_compose_content = goal_handle.request.request
        with open('/tmp/docker-compose.yml', 'w') as f:
            f.write(docker_compose_content)
        output = subprocess.check_output
        ('docker compose -f /tmp/docker-compose.yml up -d', 
        shell=True)
        goal_handle.succeed()
        result = DockerLauncher.Result()
        result.response = output.decode()
        return result


def main(args=None):
    rclpy.init(args=args)

    launch_action_server = LaunchActionServer()

    rclpy.spin(launch_action_server)


if __name__ == '__main__':
    main()
