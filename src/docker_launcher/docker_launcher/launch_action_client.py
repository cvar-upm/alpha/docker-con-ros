import sys

import rclpy
from rclpy.action import ActionClient
from rclpy.node import Node

from docker_launcher_interfaces.action import DockerLauncher


class LaunchActionClient(Node):

    def __init__(self):
        super().__init__('launch_action_client')
        self._action_client = ActionClient(self, DockerLauncher, 'DockerLauncher')

    def send_goal(self, request):
        goal_msg = DockerLauncher.Goal()
        goal_msg.request = request

        self._action_client.wait_for_server()

        self._send_goal_future = 
        self._action_client.send_goal_async(goal_msg)

        self._send_goal_future.add_done_callback
        (self.goal_response_callback)

    def goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected :(')
            return

        self.get_logger().info('Goal accepted :)')

        self._get_result_future = 
        goal_handle.get_result_async()

        self._get_result_future.add_done_callback
        (self.get_result_callback)

    def get_result_callback(self, future):
        result = future.result().result
        self.get_logger().info(result.response)
        rclpy.shutdown()


def leer_archivo(nombre_archivo):
    try:
        with open(nombre_archivo, 'r') as archivo:
            contenido = archivo.read()
        return contenido
    except FileNotFoundError:
        print("Archivo no encontrado.")
        return None


def main(args=None):
    rclpy.init(args=args)

    action_client = LaunchActionClient()
    nombre_archivo = sys.argv[1]
    contenido_archivo = leer_archivo(nombre_archivo)
    action_client.send_goal(contenido_archivo)

    rclpy.spin(action_client)


if __name__ == '__main__':
    main()
