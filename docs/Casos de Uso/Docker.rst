Introducción a Docker
=====================

Docker es una herramienta que utiliza tecnologías de virtualización a
nivel de sistema operativo para crear contenedores ligeros y portátiles.
A diferencia de las máquinas virtuales (VMs), que requieren un
hipervisor y un sistema operativo completo por cada instancia, los
contenedores de Docker comparten el mismo núcleo del sistema operativo
del host, lo que los hace más eficientes en términos de recursos.

Ventajas de Docker
------------------

1. **Portabilidad**: Los contenedores incluyen todas las dependencias
   necesarias para ejecutar una aplicación, lo que garantiza que
   funcionen de la misma manera en diferentes entornos.
2. **Escalabilidad**: Facilita la escalabilidad horizontal al permitir
   la rápida creación de múltiples instancias de contenedores.
3. **Aislamiento**: Cada contenedor opera de manera independiente,
   evitando conflictos entre aplicaciones y sus dependencias.
4. **Eficiencia**: Los contenedores son ligeros y utilizan menos
   recursos en comparación con las máquinas virtuales.

¿Qué es una imagen de Docker?
-----------------------------

Una imagen de Docker es un archivo inmutable que contiene el código, las
dependencias, las bibliotecas y el sistema de archivos necesarios para
ejecutar una aplicación. Las imágenes son el componente fundamental para
crear contenedores en Docker.

Componentes de una imagen Docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. **Base**: Una imagen base puede ser un sistema operativo minimalista
   como Alpine o una distribución más completa como Ubuntu.
2. **Capas**: Cada instrucción en un Dockerfile crea una nueva capa en
   la imagen. Estas capas se almacenan en caché y se reutilizan, lo que
   optimiza el tiempo de construcción y el uso de recursos.
3. **Dockerfile**: Un archivo de texto que contiene las instrucciones
   para construir una imagen Docker. Ejemplo de instrucciones incluyen
   ``FROM`` para especificar la imagen base, ``COPY`` para copiar
   archivos y ``RUN`` para ejecutar comandos.

Creación de una imagen Docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para crear una imagen Docker, se escribe un Dockerfile con las
instrucciones necesarias. Por ejemplo:

.. code:: dockerfile

       ARG ROS_DISTRO=iron
       FROM osrf/ros:${ROS_DISTRO}-desktop
       
       SHELL ["/bin/bash", "-c"]
       
       WORKDIR /app
       # Copia el src al contenedor
       COPY src ros2_ws/src
       # Hace el build dentro del contenedor
       RUN cd ros2_ws && source /opt/ros/${ROS_DISTRO}/setup.bash && colcon build
       
       COPY docker/ros_entrypoint.sh /
       RUN chmod +x /ros_entrypoint.sh
       
       ENTRYPOINT ["/ros_entrypoint.sh"]
       CMD ["bash"]

Luego, se construye la imagen usando el comando:

.. code:: bash

   docker build -t mi_imagen:latest .

Redes Docker: Tipos y Funcionamiento
------------------------------------

Docker ofrece varias opciones para la creación y gestión de redes, permitiendo que los contenedores se comuniquen entre sí y con el mundo exterior. A continuación, se explica cómo funcionan las redes en Docker y los diferentes tipos disponibles.

Funcionamiento de las Redes Docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las redes Docker permiten la comunicación entre contenedores, ya sea en el mismo host o en diferentes hosts. Docker administra estas redes de manera automática, proporcionando aislamiento y control sobre la conectividad entre contenedores.

Tipos de Redes Docker
~~~~~~~~~~~~~~~~~~~~~

1. **Bridge (Puente)**:

   - **Descripción**: Es el tipo de red predeterminado cuando se inicia un contenedor sin especificar una red. Crea una red de puente interna en el host de Docker.

   - **Uso**: Ideal para aplicaciones que funcionan en un solo host y requieren comunicación entre contenedores.

   - **Características**: Los contenedores en la misma red de puente pueden comunicarse entre sí usando sus nombres o direcciones IP asignadas dentro de esa red.

   **Ejemplo de creación**:

   .. code-block:: bash

      docker network create my_bridge_network

2. **Host**:

   - **Descripción**: Utiliza la pila de red del host directamente. En lugar de asignar una dirección IP separada al contenedor, este comparte la red del host.

   - **Uso**: Útil para aplicaciones que necesitan rendimiento de red máximo o cuando es necesario evitar la sobrecarga de la NAT (traducción de direcciones de red).

   - **Características**: El contenedor se comporta como si estuviera ejecutándose en el host, compartiendo su dirección IP y puertos.

   **Ejemplo de creación**:

   .. code-block:: bash

      docker run --network host my_container

3. **None**:

   - **Descripción**: Deshabilita la red para el contenedor. No se configura ninguna interfaz de red dentro del contenedor, proporcionando aislamiento completo de red.

   - **Uso**: Adecuado para tareas donde no se necesita comunicación de red.

   - **Características**: El contenedor no tiene acceso a ninguna red.

   **Ejemplo de creación**:

   .. code-block:: bash

      docker run --network none my_container

4. **Overlay**:

   - **Descripción**: Permite la creación de redes que abarcan múltiples hosts Docker. Utiliza un backend de red subyacente para conectar contenedores en diferentes máquinas.

   - **Uso**: Ideal para aplicaciones distribuidas y servicios que necesitan comunicación entre contenedores en diferentes hosts.

   - **Características**: Utilizado en entornos Docker Swarm y Kubernetes para proporcionar redes multihost.

   **Ejemplo de creación**:

   .. code-block:: bash

      docker network create --driver overlay my_overlay_network

5. **Macvlan**:

   - **Descripción**: Asigna una dirección MAC específica a cada contenedor, haciéndolo parecer como un dispositivo físico en la red. Utiliza un rango de direcciones IP disponible en la red física.

   - **Uso**: Útil para integrarse con redes físicas existentes y cuando se necesita un control más granular sobre la configuración de red.

   - **Características**: Los contenedores pueden tener direcciones IP asignadas directamente desde la red física, permitiendo comunicación directa con otros dispositivos en la misma red.

   **Ejemplo de creación**:

   .. code-block:: bash

      docker network create -d macvlan \
         --subnet=192.168.1.0/24 \
         --gateway=192.168.1.1 \
         -o parent=eth0 my_macvlan_network

6. **IPvlan**:

   - **Descripción**: Similar a Macvlan, pero con una configuración y administración más simple. Ofrece modos de operación L2 (capa 2) y L3 (capa 3).

   - **Uso**: Adecuado para entornos donde se requiere un alto rendimiento de red y una configuración de red simple.

   - **Características**: Permite asignar múltiples direcciones IP a un contenedor, proporcionando flexibilidad en la administración de red.

   **Ejemplo de creación**:

   .. code-block:: bash

      docker network create -d ipvlan \
         --subnet=192.168.1.0/24 \
         --gateway=192.168.1.1 \
         -o parent=eth0 my_ipvlan_network

Docker Compose
--------------

Docker Compose es una herramienta que simplifica la definición y gestión
de aplicaciones multi-contenedor. Con Docker Compose, se pueden definir,
ejecutar y escalar aplicaciones utilizando un archivo YAML
(``docker-compose.yml``).

Beneficios de Docker Compose
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. **Facilidad de uso**: Permite definir servicios, redes y volúmenes en
   un solo archivo.
2. **Automatización**: Facilita la automatización de tareas de
   desarrollo y pruebas al permitir el despliegue de entornos completos
   con un solo comando.
3. **Consistencia**: Garantiza que los entornos de desarrollo, prueba y
   producción sean consistentes.

Estructura de un archivo docker-compose.yml
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un archivo ``docker-compose.yml`` típico puede parecerse a esto:

.. code:: yaml

   services:
         core:
           # imagen a utilizar
           image: ros_core:latest
           # nombre del contenedor
           container_name: ros_core
           # arrancará aunque reiniciemos el sistema
           restart: always
           # comando a ejecutar
           command: ros2 run demo_node demo
           volumes:
           # path máquina local : path contenedor
             - /path/to/data:/data
           networks:
             vlan:
               # dirección IP fuera del rango DHCP
               ipv4_address: 192.168.1.205
         networks:
         vlan:
           driver: macvlan
           driver_opts:
             # puerto internet
             parent: eth0
           ipam:
             config:
               - subnet: 192.168.1.0/24
                 gateway: 192.168.1.1

Uso de Docker Compose
~~~~~~~~~~~~~~~~~~~~~

Para iniciar todos los servicios definidos en un archivo
``docker-compose.yml``, se utiliza el comando:

.. code:: bash

   docker compose up

Esto crea y arranca todos los contenedores, redes y volúmenes definidos
en el archivo. Para detener y eliminar estos recursos, se usa:

.. code:: bash

   docker compose down

Estructura de carpetas
----------------------

.. code-block:: bash

    ros2_ws/
    ├── docker/
    │   ├── Dockerfile
    │   ├── ros_entrypoint.sh
    │   ├── docker-compose.yml
    ├── src/
    │   ├── package1/
    │   └── package2/
    ├── README.md


Descripción
~~~~~~~~~~~

- **ros2_ws/**: Directorio raíz del proyecto.
- **docker/**: Carpeta que contiene archivos relacionados con Docker.

  - **Dockerfile**: Archivo para construir la imagen Docker.
  - **ros_entrypoint.sh**: Script de entrada para inicializar el entorno ROS en el contenedor.
  - **docker-compose.yml**: Archivo de configuración de Docker Compose para definir y ejecutar servicios multi-contenedor.
- **src/**: Carpeta que contiene el código fuente de los paquetes ROS.

  - **package1/** y **package2/**: Ejemplo de paquetes ROS que forman parte del workspace.
- **README.md**: Archivo de documentación del proyecto.


Estructura de carpetas en el contenedor
---------------------------------------

.. code-block:: bash

     /ros2_ws/
     ├── src/
     │   ├── package1/
     │   └── package2/



Comandos para Construir y Ejecutar la Imagen Docker
---------------------------------------------------

1. **Navegar a la Raíz del Proyecto**:
   Abre una terminal y navega al directorio raíz del proyecto:

   .. code-block:: bash

      cd path/to/ros2_ws

2. **Construir la Imagen Docker**:
   Ejecuta el comando `docker build` desde la raíz del proyecto, especificando la ruta al Dockerfile:

   .. code-block:: bash

      docker build -t ros_desktop -f docker/Dockerfile .

   - ``-t ros_desktop``: Etiqueta (nombre) que le das a tu imagen.
   - ``-f docker/Dockerfile``: Especifica la ruta al Dockerfile.
   - ``.``: Contexto de construcción, que es el directorio actual (`ros2_ws`).

3. **Ejecutar la Imagen Docker**:
   Una vez que la imagen se ha construido correctamente, puedes ejecutarla con el siguiente comando:

   .. code-block:: bash

      docker run -it ros_desktop:latest bash

   - ``-it``: Hace que la terminal sea interactiva.
   - ``ros_desktop:latest``: Especifica la imagen y la etiqueta (en este caso ``latest``).
   - ``bash``: Comando a ejecutar dentro del contenedor, lo que te dará una terminal Bash.

4. **Iniciar los Servicios con Docker Compose**:
   Si también estás usando Docker Compose y tienes un archivo `docker-compose.yml` en `docker/`, puedes iniciar los servicios con:

   .. code-block:: bash

      cd docker
      docker compose up -d

   Esto ejecutará los contenedores en segundo plano (``-d``).

Acceso al Entorno
-----------------

- **Acceder al Contenedor**:
  Después de ejecutar el contenedor, tendrás acceso a una terminal Bash dentro del contenedor donde puedes ejecutar comandos ROS.

- **Acceso al Escritorio Remoto** (si estás usando noVNC):
  Abre tu navegador web y navega a `http://localhost:8080/vnc.html` para acceder al escritorio remoto proporcionado por el servicio noVNC.

Limpieza
--------

Para detener el contenedor creado con el Dockerfile:

.. code-block:: bash

   docker stop <nombre_o_id_del_contenedor>

Para detener y eliminar los contenedores y redes creados por Docker Compose:

.. code-block:: bash

   docker compose down

Para eliminar la imagen Docker si ya no la necesitas:

.. code-block:: bash

   docker rmi ros_desktop:latest
