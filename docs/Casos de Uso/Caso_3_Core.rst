ROS Core
=============
+------------------------+---------------------------------------------+
| 3                      | ROS Core                                    |
+========================+=============================================+
| Configuración          | Imagen:                                     |
|                        | ``ros:iron-ros-core-jammy``\ Versión:       |
|                        | ``iron``\                                   |
+------------------------+---------------------------------------------+
| Paquetes incluidos en  | ``ros-iron-ros-core=0.10.0-3``              |
| la imagen              |                                             |
+------------------------+---------------------------------------------+
| Tamaño de la imagen    | 495.43 MB                                   |
+------------------------+---------------------------------------------+

Resumen
-------

La imagen básica de ROS Core incluye solo los componentes esenciales de
ROS y excluye herramientas adicionales como Colcon, necesaria para
compilar paquetes de ROS 2. Está diseñada para ejecutar aplicaciones y
realizar pruebas con el núcleo de ROS, reduciendo el tamaño y la
complejidad del entorno. Sin embargo, no es adecuada para el desarrollo
activo de software, ya que no permite compilar nuevos paquetes.

Utilidad
--------

Se podría utilizar para la ejecución y validación de nodos ROS ya
compilados, pruebas automatizadas en pipelines de integración continua y
despliegue en producción en sistemas robóticos que no requieren la
compilación de código.

Uso y Ciclo de Vida
--------

El Dockerfile configura un entorno base para ROS (Robot Operating System) versión ``iron``, utilizando la imagen base ``ros:iron-ros-core-jammy``.
Esta imagen contiene solo los componentes esenciales de ROS, sin herramientas adicionales como Colcon.
Es ideal para ejecutar aplicaciones y realizar pruebas con el núcleo de ROS en un entorno ligero.
Para usar este Dockerfile, navega al directorio raíz del proyecto y ejecuta ``docker build -t ros_core -f docker/Dockerfile .`` para construir la imagen. Luego, para iniciar un contenedor, usa ``docker run -it ros_core bash`` para ejecutar y probar aplicaciones ROS.

El archivo Docker Compose configura un entorno multi-contenedor para ejecutar nodos ROS ya compilados.
Define un servicio `core` que utiliza la imagen `ros_core` y ejecuta un nodo ROS de demostración.
El servicio monta un volumen local en el contenedor y utiliza una red `macvlan` para la comunicación.
Para ejecutar el Docker Compose, navega al directorio `docker` y ejecuta ``docker-compose up -d`` para iniciar el servicio en segundo plano.


Dockerfile
---------

.. code:: dockerfile

       ARG ROS_DISTRO=iron
       FROM ros:${ROS_DISTRO}-ros-core-jammy

       SHELL ["/bin/bash", "-c"]

       CMD ["bash"]

Docker Compose
--------------

.. code:: yaml

   services:
         core:
           # imagen a utilizar
           image: ros_core:latest
           # nombre del contenedor
           container_name: ros_core
           # arrancará aunque reiniciemos el sistema
           restart: always
           # comando a ejecutar
           command: ros2 run demo_node demo
           volumes:
           # path máquina local : path contenedor
             - ../src:/ros2_ws/src
           networks:
             vlan:
               # dirección IP fuera del rango DHCP
               ipv4_address: 192.168.1.205
         networks:
         vlan:
           driver: macvlan
           driver_opts:
             # puerto internet
             parent: eth0
           ipam:
             config:
               - subnet: 192.168.1.0/24
                 gateway: 192.168.1.1

Comandos
--------

.. code:: bash

       # Construir la imagen Docker
       docker build -t ros_core -f docker/Dockerfile .

       # Ejecutar un contenedor interactivo desde la imagen construida
       docker run -it ros_core:latest bash

        # Construir los servicios definidos en docker-compose.yml
        docker compose -f docker/docker-compose.yml build

        # Iniciar los servicios definidos en docker-compose.yml
        docker compose -f docker/docker-compose.yml up -d
