ROS Desktop
===========
+--------------+----------------------------------------------------------+
| 2            | ROS Desktop                                              |
+==============+==========================================================+
| Configuración| Imagen: ``osrf/ros:iron-desktop``\ Versión: ``iron``\    |
+--------------+----------------------------------------------------------+
| Paquetes     | ``ros-iron-desktop=0.10.0-                               |
| incluidos    | 3``\ \ ``git``\ \ ``python3-colcon-common-extensions``\  |
| en la        | ``python3-colco                                          |
| imagen       | n-mixin``\ \ ``python3-rosdep``\ \ ``python3-vcstool``\  |
+--------------+----------------------------------------------------------+
| Tamaño de    | 3.47 GB                                                  |
| la imagen    |                                                          |
+--------------+----------------------------------------------------------+

Resumen
-------

Este caso de uso utiliza la imagen de ROS Desktop, configurada con la
imagen ``osrf/ros:iron-desktop`` basada en la versión iron de ROS. La
imagen contiene varios paquetes esenciales como ``git``, ``python3`` y
``colcon``. Esta imagen está diseñada para proporcionar un entorno de
escritorio completo para el desarrollo y la ejecución de aplicaciones
ROS, incluyendo la capacidad de conectarse a través de un escritorio
remoto utilizando noVNC.

Utilidad
--------

La imagen de Docker ROS Desktop es especialmente útil para
desarrolladores que necesitan un entorno gráfico completo para trabajar
con aplicaciones ROS. Con la inclusión de herramientas de desarrollo y
paquetes de escritorio, los usuarios pueden compilar, gestionar y
desarrollar nuevos paquetes ROS con una interfaz gráfica accesible
remotamente. Esto facilita la creación y prueba de aplicaciones
robóticas en un entorno gráfico, permitiendo realizar pruebas visuales y
depuraciones más efectivas.


Uso y Ciclo de Vida
--------

El Dockerfile configura un entorno de desarrollo para ROS (Robot Operating System) versión ``iron``.
Utiliza la imagen base ``osrf/ros:iron-desktop``, copia el código fuente al contenedor, construye el workspace con colcon y configura un script de entrada ``ros_entrypoint.sh`` para iniciar el contenedor.
Para usar este Dockerfile, navega al directorio raiz del proyecto y ejecuta ``docker build -t ros_desktop -f docker/Dockerfile .`` para construir la imagen.
Luego, para iniciar un contenedor con: ``docker run -it ros_desktop`` para desarrollar y ejecutar aplicaciones ROS.

El archivo Docker Compose configura un entorno multi-contenedor para ejecutar una aplicación ROS con un servicio de escritorio remoto noVNC.
Define dos servicios: turtlesim, que construye y ejecuta una aplicación ROS, y novnc, que proporciona acceso remoto a la interfaz gráfica.
El servicio turtlesim se basa en una imagen de Docker creada a partir de un Dockerfile y ejecuta un nodo de ROS, mientras que novnc expone un puerto para el acceso remoto.
Para ejecutar el Docker Compose se necesita ejecutar desde la carpeta docker ``docker-compose up -d`` para iniciar ambos servicios en segundo plano.







Dockerfile
---------

.. code:: dockerfile

   # Distribución
   ARG ROS_DISTRO=iron
   FROM osrf/ros:${ROS_DISTRO}-desktop

   SHELL ["/bin/bash", "-c"]

   WORKDIR /app
   # Copia el src al contenedor
   COPY src ros2_ws/
   # Hace el build dentro del contenedor
   RUN cd ros2_ws && source /opt/ros/${ROS_DISTRO}/setup.bash && colcon build

   COPY docker/ros_entrypoint.sh /
   RUN chmod +x /ros_entrypoint.sh

   ENTRYPOINT ["/ros_entrypoint.sh"]
   CMD ["bash"]

Docker Compose
--------------

.. code:: yaml

   services:
       turtlesim:
           image: demo:latest
           pull_policy: always
           build:
               context: ../
               dockerfile: ./docker/Dockerfile
               args:
                 ROS_DISTRO: iron
           command: ros2 run turtlesim turtlesim_node
           environment:
               - DISPLAY=novnc:0.0
           networks:
             - ros2
           depends_on:
             - novnc
           volumes:
             - ../src:/ros2_ws/src
           restart: always
       novnc:
         image: theasp/novnc:latest
         environment:
           # Adjust to your screen size
           - DISPLAY_WIDTH=1600
           - DISPLAY_HEIGHT=968
         ports:
           - "8080:8080"
         networks:
           - ros2
         restart: always
   networks:
     ros2:
       name: ros2

Comandos
--------

.. code:: bash

   # Construir la imagen Docker
   docker build -t ros_desktop -f docker/Dockerfile .

   # Ejecutar un contenedor interactivo desde la imagen construida
   docker run -it ros_desktop:latest bash

   # Construir los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml build

   # Iniciar los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml up -d

Para acceder al escritorio remoto de la imagen debemos de acceder a esta
ruta desde nuestro navegador web: ``http://localhost:8080/vnc.html``
