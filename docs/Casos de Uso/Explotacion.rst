Explotación
===========

Introducción
------------

La sección de Explotación se centra en los casos de uso para la implementación y prueba del código finalizado en entornos de producción o pre-producción. En esta fase, el código ya ha pasado por todas las pruebas de desarrollo y está listo para ser ejecutado sin modificaciones adicionales. Es fundamental asegurarse de que el entorno de ejecución sea estable y reproducible, y que incluya solo los componentes necesarios para la operación del sistema.

En esta sección, aprenderemos cómo utilizar Docker para desplegar aplicaciones ROS listas para producción, incluyendo la instalación de paquetes adicionales de ROS mediante `apt` para extender la funcionalidad de nuestras aplicaciones. Veremos configuraciones de Docker Compose que permiten ejecutar múltiples servicios de manera orquestada y ejemplos de buenas prácticas para garantizar la seguridad y eficiencia de los contenedores en producción.

Cómo lanzar varios contenedores en la misma red
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Docker Compose permite definir y ejecutar aplicaciones multi-contenedor. A continuación, se explica cómo configurar un archivo `docker-compose.yml` para lanzar un `talker` y un `listener` en la misma red utilizando la red `macvlan`.

Este archivo `docker-compose.yml` configura dos servicios, `talker` y `listener`, en la misma red `macvlan`. Ambos servicios usan imágenes de ROS y se comunican a través de direcciones IP específicas en la red `macvlan`.

Archivo `docker-compose.yml`
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

   services:
     # Servicio talker
     talker:
       # Imagen de Docker a utilizar
       image: ros:foxy-ros-base
       # Nombre del contenedor
       container_name: ros_talker
       # Comando a ejecutar cuando el contenedor se inicie
       command: ros2 run demo_nodes_cpp talker
       # Conexión a la red macvlan con una IP específica
       networks:
         my_macvlan_network:
           ipv4_address: 192.168.1.10

     # Servicio listener
     listener:
       # Imagen de Docker a utilizar
       image: ros:foxy-ros-base
       # Nombre del contenedor
       container_name: ros_listener
       # Comando a ejecutar cuando el contenedor se inicie
       command: ros2 run demo_nodes_cpp listener
       # Conexión a la red macvlan con una IP específica
       networks:
         my_macvlan_network:
           ipv4_address: 192.168.1.11

   networks:
     # Definición de la red macvlan
     my_macvlan_network:
       # Especifica el controlador de red macvlan
       driver: macvlan
       driver_opts:
         # Define la interfaz de red del host que se usará
         parent: eth0
       ipam:
         config:
           - subnet: 192.168.1.0/24
             gateway: 192.168.1.1

Casos de Uso
------------

.. toctree::
   :maxdepth: 2

   Caso_1_Base
   Caso_2_ARM
   Caso_3_Linux