ROS con Linux Desde Cero
================
+------------------------+---------------------------------------------+
| 3                      | Linux De Cero                               |
+========================+=============================================+
| Configuración          | Imagen:                                     |
|                        | ``ubuntu:20.04``\                           |
|                        | Versión: ``iron``\                          |
+------------------------+---------------------------------------------+
| Paquetes incluidos en  | ``ros-iron-desktop``\                       |
| la imagen              | ``ros-dev-tools``                           |
+------------------------+---------------------------------------------+
| Tamaño de la imagen    | 3.7 GB                                      |
+------------------------+---------------------------------------------+

Resumen
-------

Este caso de uso utiliza la imagen base de Ubuntu 20.04 para configurar un entorno ROS 2 Iron.
La imagen incluye los paquetes esenciales ``ros-iron-desktop`` y ``ros-dev-tools``, lo que proporciona un entorno completo
para el desarrollo y ejecución de aplicaciones ROS 2. El Dockerfile también configura el entorno de locales y agrega las
claves necesarias para instalar ROS 2 desde los repositorios oficiales.

Utilidad
--------

La imagen Docker de ROS Desktop Iron es particularmente útil para desarrolladores que trabajan con ROS 2 Iron.
Proporciona un entorno preconfigurado que incluye herramientas de desarrollo y paquetes de escritorio, lo que facilita
la compilación, gestión y desarrollo de nuevos paquetes ROS 2. Esta configuración permite una rápida puesta en marcha
y asegura la consistencia en los entornos de desarrollo y producción.

Uso y Ciclo de Vida
-------------------

El Dockerfile configura un entorno de desarrollo completo para ROS 2 Iron, utilizando Ubuntu 20.04 como base.
El proceso de configuración incluye la instalación de locales, adición de repositorios ROS 2 y la instalación de
paquetes esenciales. Para usar este Dockerfile, navega al directorio raíz del proyecto y ejecuta ``docker build -t ros_iron_desktop -f docker/Dockerfile .``
para construir la imagen. Luego, para iniciar un contenedor, usa ``docker run -it ros_iron_desktop bash`` para desarrollar y ejecutar aplicaciones ROS 2.

El archivo Docker Compose define un servicio ``talker`` que utiliza la imagen ``ros_iron_desktop`` y ejecuta el nodo ``talker`` del paquete ``demo_nodes_cpp``.
El servicio monta un volumen local en el contenedor y se conecta a la red ``ros2``. Para ejecutar el Docker Compose, navega al directorio ``docker`` y ejecuta ``docker-compose up -d``
para iniciar el servicio en segundo plano.

Dockerfile
----------

.. code-block:: dockerfile

   FROM ubuntu:20.04
   ARG ROS_DISTRO=iron
   SHELL ["/bin/bash", "-c"]
   ARG DEBIAN_FRONTEND=noninteractive
   ENV TZ=Etc/UTC
   USER root

   RUN apt update && apt install locales && \
       locale-gen en_US en_US.UTF-8 && \
       update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 && \
       export LANG=en_US.UTF-8 && \
       apt install software-properties-common -y && \
       add-apt-repository universe && \
       apt update && apt install curl -y && \
       curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg && \
       echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null && \
       apt update && apt upgrade -y && apt install ros-${ROS_DISTRO}-desktop -y && apt install ros-dev-tools -y && \
       apt clean autoclean && apt autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}

   WORKDIR /ros2_ws

   COPY src .
   RUN cd ros2_ws && source /opt/ros/${ROS_DISTRO}/setup.bash && colcon build

   COPY docker/ros_entrypoint.sh /
   RUN chmod +x /ros_entrypoint.sh

   ENTRYPOINT ["/ros_entrypoint.sh"]

   CMD ["bash"]

Docker Compose
--------------

.. code-block:: yaml

   services:
       talker:
           image: osrf/ros:iron-desktop
           pull_policy: always
           build:
               context: ../
               dockerfile: ./docker/Dockerfile
               args:
                 ROS_DISTRO: iron
           command: ros2 run demo_nodes_cpp talker
           networks:
             - ros2
           volumes:
             - ../src:/ros2_ws/src
           restart: always

   networks:
     ros2:
       name: ros2

Comandos
--------

Para construir y ejecutar la imagen y los contenedores definidos, utiliza los siguientes comandos:

.. code-block:: bash

   # Construir la imagen Docker
   docker build -t ros_iron_desktop -f docker/Dockerfile .

   # Ejecutar un contenedor interactivo desde la imagen construida
   docker run -it ros_iron_desktop bash

   # Construir los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml build

   # Iniciar los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml up -d
