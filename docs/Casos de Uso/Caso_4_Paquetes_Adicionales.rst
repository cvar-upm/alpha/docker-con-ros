ROS con Paquetes Adicionales
=============
+------------------------+---------------------------------------------+
| 4                      | ROS con Paquetes Adicionales                |
+========================+=============================================+
| Configuración          | Imagen:                                     |
|                        | ``ros:iron-ros-core-jammy``\ Versión:       |
|                        | ``iron``\                                   |
+------------------------+---------------------------------------------+
| Paquetes incluidos en  | ``ros-iron-ros-core=0.10.0-3``              |
| la imagen              |                                             |
+------------------------+---------------------------------------------+
| Tamaño de la imagen    | 495.43 MB                                   |
+------------------------+---------------------------------------------+

Resumen
-------

Este caso de uso utiliza la imagen de ROS Core, configurada con la imagen
``ros:iron-ros-core-jammy``. La imagen incluye el paquete esencial ``ros-iron-ros-core`` y
se ha extendido para incluir herramientas adicionales como `rsync` y el paquete `demo_nodes_cpp`
de ROS, que proporciona nodos de ejemplo como `talker` y `listener`. Esta configuración permite
la sincronización de archivos y la ejecución de nodos adicionales en ROS.

Utilidad
--------

La imagen de Docker ROS con Paquetes Adicionales es útil para desarrolladores y equipos que necesitan un entorno de ROS
enriquecido con herramientas adicionales. Con `rsync`, los usuarios pueden sincronizar archivos y directorios de manera eficiente entre
diferentes ubicaciones, facilitando la transferencia de datos en entornos distribuidos. Además, el paquete `demo_nodes_cpp` permite
probar y demostrar rápidamente la funcionalidad de ROS, proporcionando un entorno completo para desarrollo y pruebas.

Uso y Ciclo de Vida
-------------------

El Dockerfile configura un entorno base para ROS (Robot Operating System) versión ``iron``, utilizando la imagen base
``ros:iron-ros-core-jammy``. Además de los componentes esenciales de ROS, instala `rsync` y el paquete `demo_nodes_cpp`.
Para usar este Dockerfile, navega al directorio raíz del proyecto y ejecuta ``docker build -t ros_paquetes_adicionales -f docker/Dockerfile .``
para construir la imagen. Luego, para iniciar un contenedor, usa ``docker run -it ros_paquetes_adicionales bash`` para desarrollar y ejecutar aplicaciones ROS.

El archivo Docker Compose configura un entorno multi-contenedor para ejecutar nodos ROS de ejemplo. Define un servicio `core` que utiliza la imagen `ros_paquetes_adicionales`
y ejecuta el nodo `talker` del paquete `demo_nodes_cpp`. El servicio monta un volumen local en el contenedor y utiliza una red `macvlan` para la comunicación.
Para ejecutar el Docker Compose, navega al directorio `docker` y ejecuta ``docker-compose up -d`` para iniciar el servicio en segundo plano.

Dockerfile
---------

.. code:: dockerfile

   ARG ROS_DISTRO=iron
   FROM ros:${ROS_DISTRO}-ros-core-jammy

   RUN apt update && apt install -y rsync \
   apt install ros-${ROS_DISTRO}-demo-nodes-cpp \
   apt clean autoclean && apt autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}

   SHELL ["/bin/bash", "-c"]

   CMD ["bash"]

Docker Compose
--------------

.. code:: yaml

   services:
     core:
       image: ros_paquetes_adicionales:latest
       container_name: ros_paquetes
       restart: always
       command: ros2 run demo_nodes_cpp talker
       volumes:
         - ../src:/ros2_ws/src
       networks:
         vlan:
           ipv4_address: 192.168.1.60

   networks:
     vlan:
       driver: macvlan
       driver_opts:
         parent: eth0
       ipam:
         config:
           - subnet: "192.168.1.0/24"
             gateway: "192.168.1.1"

Comandos
--------

.. code:: bash

   # Construir la imagen Docker
   docker build -t ros_paquetes_adicionales -f docker/Dockerfile .

   # Ejecutar un contenedor interactivo desde la imagen construida
   docker run -it ros_paquetes_adicionales bash

   # Construir los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml build

   # Iniciar los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml up -d

Rsync
-----
``Rsync`` es una herramienta de sincronización de archivos que permite copiar y sincronizar archivos y directorios entre diferentes ubicaciones, ya sea en el mismo sistema o a través de la red.


Syntaxis
~~~~~~~~

Primero hay que crear un alias con este comando:
``alias drsync="rsync -e 'docker exec -i'"``

Para copiar las carpetas/ficheros se hace con este comando: (``drsync -av`` carpeta
contenedor:destino): ``drsync -av ../src/ docker-test-1:/appp``
