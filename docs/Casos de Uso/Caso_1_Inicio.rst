ROS Inicio
========
+-------------+----------------------------------------------------------+
| 1           | ROS Inicio                                               |
+=============+==========================================================+
|Configuración| Imagen: ``ros:iron-ros-base-jammy``\ Versión:``iron``\   |
+-------------+----------------------------------------------------------+
| Paquetes    | ``ros-iron-ros-core=0.10.0-3``\                          |
| incluidos   | ``git``\ \ ``python3-colcon-common-extensions``\         |
| en la       | ``python3-colco                                          |
| imagen      | n-mixin``\ \ ``python3-rosdep``\ \ ``python3-vcstool``\  |
+-------------+----------------------------------------------------------+
| Tamaño de   | 777.46 MB                                                |
| la imagen   |                                                          |
+-------------+----------------------------------------------------------+

Resumen
-------

La imagen de ROS Inicio se basa en `ros:iron-ros-base-jammy` e incluye varios paquetes esenciales para el desarrollo y ejecución de aplicaciones ROS.
Esta configuración proporciona un entorno de desarrollo básico pero funcional, con herramientas como `colcon` para la compilación de paquetes y `git` para el control de versiones.
Además, se instala el paquete `demo_nodes_cpp`, que contiene nodos de ejemplo para facilitar el aprendizaje y la demostración de la funcionalidad de ROS.

Utilidad
--------

Esta imagen es útil para desarrolladores que están comenzando con ROS y necesitan un entorno listo para el desarrollo y pruebas de aplicaciones.
La inclusión de herramientas de desarrollo esenciales y nodos de ejemplo permite a los usuarios aprender y experimentar con ROS de manera eficiente.
Es adecuada tanto para entornos de desarrollo locales como para la integración continua en pipelines de CI/CD.

Uso y Ciclo de Vida
-------------------

El Dockerfile configura un entorno base para ROS, utilizando la imagen ros:iron-ros-base-jammy y añadiendo herramientas de desarrollo y nodos de ejemplo.
Además, cualquier paquete ROS adicional se puede instalar utilizando `apt install`  , lo que proporciona flexibilidad para personalizar el entorno según las necesidades del proyecto.


Dockerfile
---------

.. code:: dockerfile

    ARG ROS_DISTRO=iron
    FROM ros:${ROS_DISTRO}-ros-base-jammy

    # Se instala el paquete de ejemplo de ros demo_nodes_cpp
    RUN apt update && apt install -y ros-${ROS_DISTRO}-demo-nodes-cpp && \
        apt clean autoclean && apt autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}

    SHELL ["/bin/bash", "-c"]

    CMD ["bash"]

Docker Compose
--------------

.. code:: yaml

    services:
      inicial:
        image: ros_inicial:latest
        container_name: ros_inicial
        volumes:
          - ../src:/ros2_ws/src
        network_mode: host

Comandos
--------

.. code:: bash

   # Construir la imagen Docker
   docker build -t ros_inicial -f docker/Dockerfile .

   # Ejecutar un contenedor interactivo desde la imagen construida
   docker run -it ros_inicial:latest bash

   # Construir los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml build

   # Ejecutar un contenedor específico definido en docker-compose.yml
   docker compose -f docker/docker-compose.yml run inicial bash
