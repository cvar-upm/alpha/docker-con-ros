ROS ARM
=======
+------------------------+---------------------------------------------+
| 2                      | ROS arm64v8                                 |
+========================+=============================================+
| Configuración          | Imagen:                                     |
|                        | ``ros:noetic-ros-core-focal``\ Versión:     |
|                        | ``iron``\                                   |
+------------------------+---------------------------------------------+
| Paquetes incluidos en  | ``ros-iron-ros-core=0.10.0-3``              |
| la imagen              |                                             |
+------------------------+---------------------------------------------+
| Tamaño de la imagen    | 774.47 MB                                   |
+------------------------+---------------------------------------------+

Resumen
-------

Este caso de uso utiliza la imagen de ROS arm64v8, configurada con la imagen
``ros:noetic-ros-core-focal``. La imagen contiene varios paquetes esenciales
como ``ros-iron-ros-core``. Esta imagen está diseñada para proporcionar un
entorno básico de ROS en arquitecturas ARM, facilitando el desarrollo y la
ejecución de aplicaciones ROS en dispositivos con esta arquitectura.

Utilidad
--------

La imagen de Docker ROS ARM es especialmente útil para desarrolladores que
trabajan con dispositivos basados en la arquitectura ARM, como Raspberry Pi.
Proporciona un entorno ligero y eficiente para ejecutar y desarrollar aplicaciones ROS
en estos dispositivos. Con esta imagen, los usuarios pueden aprovechar las
herramientas básicas de ROS para construir y probar aplicaciones robóticas en plataformas ARM,
permitiendo una mayor flexibilidad y portabilidad en el desarrollo de soluciones robóticas.

Uso y Ciclo de Vida
-------------------

El Dockerfile configura un entorno base para ROS (Robot Operating System) en arquitecturas ARM,
utilizando la imagen base ``ros:noetic-ros-core-focal``. Actualiza los paquetes del sistema y
establece el shell por defecto como `/bin/bash`. Para usar este Dockerfile, navega al directorio raíz del
proyecto y ejecuta ``docker build -t ros_desktop_arm -f docker/Dockerfile .`` para construir la imagen.
Luego, para iniciar un contenedor, usa ``docker run -it ros_desktop_arm bash`` para desarrollar y
ejecutar aplicaciones ROS en dispositivos ARM.

El archivo Docker Compose configura un entorno multi-contenedor para ejecutar nodos ROS.
Define un servicio: `talker` y `listener`, que ejecuta el nodo `talker` y `listener` del paquete `demo_nodes_cpp`.
El servicio utiliza la imagen `ros_desktop_arm` y comparte la red del host para facilitar la comunicación
entre nodos. Para ejecutar el Docker Compose, navega al directorio `docker` y ejecuta ``docker-compose up -d``
para iniciar el servicio en segundo plano.


Dockerfile
---------

.. code:: dockerfile

   FROM ros:noetic-ros-core-focal

   SHELL ["/bin/bash", "-c"]

   CMD ["bash"]

Docker Compose
--------------

.. code:: yaml

   services:
     talker:
       image: ros_desktop_arm:latest
       command: ros2 run demo_nodes_cpp talker
       network_mode: host
     listener:
       image: ros_base:latest
       command: ros2 run demo_nodes_cpp listener
       network_mode: host

Comandos
--------

.. code::  bash

   # Construir la imagen Docker
   docker build -t ros_desktop_arm -f docker/Dockerfile .

   # Ejecutar un contenedor interactivo desde la imagen construida
   docker run -it ros_desktop_arm bash

   # Construir los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml build

   # Iniciar los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml up -d
