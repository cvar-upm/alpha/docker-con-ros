ROS Base
========
+-------------+----------------------------------------------------------+
| 1           | ROS Base                                                 |
+=============+==========================================================+
|Configuración| Imagen: ``ros:iron-ros-base-jammy``\ Versión:``iron``\   |
+-------------+----------------------------------------------------------+
| Paquetes    | ``ros-iron-ros-core=0.10.0-3``\                          |
| incluidos   | ``git``\ \ ``python3-colcon-common-extensions``\         |
| en la       | ``python3-colco                                          |
| imagen      | n-mixin``\ \ ``python3-rosdep``\ \ ``python3-vcstool``\  |
+-------------+----------------------------------------------------------+
| Tamaño de   | 777.46 MB                                                |
| la imagen   |                                                          |
+-------------+----------------------------------------------------------+

Resumen
-------

Este caso de uso utiliza la imagen de ROS Base, configurada con la
imagen ``ros:iron-ros-base-jammy`` basada en la versión iron de ROS. La
imagen contiene varios paquetes esenciales como ``git``, ``python3`` y
``colcon``. Estos paquetes permiten no solo ejecutar aplicaciones ROS,
sino también desarrollar y compilar nuevos paquetes ROS, proporcionando
un entorno de desarrollo más completo y funcional. En el Dockerfile también
se instala el paquete ``demo_nodes_cpp`` que contiene el ``talker y listener``.
De este modo se puede instalar cualquier paquete adicional que sea requiera.


Utilidad
--------

Esta imagen de Docker es útil para desarrolladores y equipos que
trabajan con ROS y necesitan un entorno de desarrollo robusto y
flexible. Con la inclusión de herramientas como Colcon y Git, los
usuarios pueden compilar, gestionar y desarrollar nuevos paquetes ROS
directamente dentro del contenedor. Esto facilita la creación y prueba
de aplicaciones robóticas, permitiendo realizar pruebas automatizadas y
despliegues en sistemas de producción.

Uso y Ciclo de Vida
-------------------

El Dockerfile configura un entorno base para ROS (Robot Operating System) versión ``iron``.
Utiliza la imagen base ``ros:iron-ros-base-jammy``, actualiza los paquetes del sistema e instala el paquete `demo_nodes_cpp` que incluye nodos de ejemplo `talker` y `listener`.
Para usar este Dockerfile, navega al directorio raíz del proyecto y ejecuta ``docker build -t ros_base -f docker/Dockerfile .`` para construir la imagen.
Luego, para iniciar un contenedor, usa ``docker run -it ros_base`` para  ejecutar aplicaciones ROS.

El archivo Docker Compose configura un entorno multi-contenedor para ejecutar nodos ROS. Define dos servicios: `talker`, que ejecuta el nodo `talker` del paquete `demo_nodes_cpp`, y `listener`, que ejecuta el nodo `listener` del mismo paquete. Ambos servicios utilizan la imagen `ros_base` y comparten la red del host para facilitar la comunicación entre nodos.
Para ejecutar el Docker Compose, navega al directorio `docker` y ejecuta ``docker-compose up -d`` para iniciar ambos servicios en segundo plano.



Dockerfile
---------

.. code:: dockerfile

   ARG ROS_DISTRO=iron
   FROM ros:${ROS_DISTRO}-ros-base-jammy
   # Se instala el paquete de ros demo_nodes_cpp
   RUN apt update && apt install -y ros-${ROS_DISTRO}-demo-nodes-cpp \
   apt clean autoclean && apt autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}

   SHELL ["/bin/bash", "-c"]

   CMD ["bash"]

Docker Compose
--------------

.. code:: yaml

   services:
     talker:
       image: ros_base:latest
       command: ros2 run demo_nodes_cpp talker
       network_mode: host
     listener:
       image: ros_base:latest
       command: ros2 run demo_nodes_cpp listener
       network_mode: host

Comandos
--------

.. code:: bash

   # Construir la imagen Docker
   docker build -t ros_base -f docker/Dockerfile .

   # Ejecutar un contenedor interactivo desde la imagen construida
   docker run -it ros_base:latest bash

   # Construir los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml build

   # Iniciar los servicios definidos en docker-compose.yml
   docker compose -f docker/docker-compose.yml up -d
