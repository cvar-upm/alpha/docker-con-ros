Desarrollo
===========




Introducción
----------

La sección de Desarrollo está dedicada a los casos de uso que facilitan la prueba y validación del código mientras se encuentra en las fases de desarrollo y prueba. Durante esta etapa, es crucial contar con un entorno flexible que permita realizar modificaciones rápidas, ejecutar pruebas unitarias y de integración, y depurar de manera eficiente. Utilizando Docker, podemos crear entornos de desarrollo consistentes que aseguren que el código se comporta de manera predecible en diferentes máquinas y configuraciones.

En esta sección, exploraremos cómo utilizar imágenes de Docker específicas para ROS que proporcionan un entorno de desarrollo completo con las herramientas necesarias para compilar, probar y depurar aplicaciones ROS. También veremos ejemplos de Dockerfiles y configuraciones de Docker Compose que facilitan la integración continua y el desarrollo colaborativo en equipo.


Casos de Uso
------------

.. toctree::
   :maxdepth: 2

   Caso_1_Inicio
   Caso_2_Desktop
   Caso_3_Core
   Caso_4_Paquetes_Adicionales